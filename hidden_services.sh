#!/bin/sh
#
# Get every website with hidden service (HS) enabled from Sutty's API
# and generate a Tor configuration file for it.  We do it this way
# because the original plan was to use Tor Control Protocol, but it only
# allows to create ephemeral hidden services if you don't store the
# private key separately.  HS are lost when Tor is restarted.  We don't
# want Sutty to have access to HS' private keys nor we want to device
# some algorithm to keep HS' alive when Tor restarts.
#
# So we did this that generates the persistent config file, reload Tor
# to make it generate the HS and inform Sutty of the public key / onion
# address.  This way private keys are only stored in Tor.

set -e

if test "$1" = "bootstrap" ; then
  install -dm 2755 -o tor  -g root /var/lib/tor/hidden_services
  install -dm 2755 -o root -g root /var/lib/tor/hidden_services/conf.d
  exit 0
fi

# API client
api_client () {
  local _path="$1"; shift

  curl --basic --user "${HTTP_BASIC_USER}:${HTTP_BASIC_PASSWORD}" \
       $@ "https://api.${SUTTY}${_path}"
}

# Get all sites with HS enabled
api_client "/v1/sites/hidden_services.json" | jq --raw-output .[] | while read name; do
  conf_file="/var/lib/tor/hidden_services/conf.d/${name}.conf"
  hs_dir="/var/lib/tor/hidden_services/${name}"

  # The config file
  echo "HiddenServiceDir ${hs_dir}" > "${conf_file}"
  echo "HiddenServicePort 80 nginx:80" >> "${conf_file}"
  echo "HiddenServiceEnableIntroDoSDefense 1" >> "${conf_file}"

  chmod 644 "${conf_file}"

  # Reload Tor
  cat /var/lib/tor/tor.pid | xargs -r kill -SIGHUP

  # Wait for the hidden service to be created
  while ! test -f "${hs_dir}/hostname"; do sleep 1 ; done

  # Inform the hidden service to Sutty
  api_client "/v1/sites/add_onion.json" \
             --data "name=${name}" \
             --data-urlencode "onion@${hs_dir}/hostname"
done
