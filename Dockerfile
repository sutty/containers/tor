FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

ENV SUTTY="sutty.nl"
ENV HTTP_BASIC_USER=""
ENV HTTP_BASIC_PASSWORD=""

RUN apk add --no-cache tor curl jq
COPY ./monit.conf /etc/monit.d/tor.conf
COPY ./torrc /etc/tor/torrc
COPY ./hidden_services.sh /usr/local/bin/hidden_services
RUN chmod +x /usr/local/bin/hidden_services
RUN chmod 644 /etc/tor/torrc

VOLUME /var/lib/tor
